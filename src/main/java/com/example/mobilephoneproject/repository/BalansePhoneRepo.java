package com.example.mobilephoneproject.repository;

import com.example.mobilephoneproject.entity.BalancePhoneEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface BalansePhoneRepo extends JpaRepository<BalancePhoneEntity, Integer>{
    @Query("SELECT f FROM BalancePhoneEntity f WHERE f.numberPhone = ?1")
    public BalancePhoneEntity findByNumber(Integer numberPhone);

    @Query("SELECT f FROM BalancePhoneEntity f WHERE f.nameCustomer = ?1")
    public List<BalancePhoneEntity> findByName(String nameCustomer);

}
