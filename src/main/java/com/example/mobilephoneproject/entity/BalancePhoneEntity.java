package com.example.mobilephoneproject.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@Accessors(chain = true)
public class BalancePhoneEntity {
    @Id
    @Column
    private Integer id;
    @Column
    private Integer numberPhone;
    @Column
    private String nameCustomer;
    @Column
    private Integer balance;
}
