package com.example.mobilephoneproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobilePhoneProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(MobilePhoneProjectApplication.class, args);
    }

}
