package com.example.mobilephoneproject.dto;

import lombok.Data;

import javax.persistence.Column;
@Data
public class BalancePhoneDto {
    private Integer id;
    private Integer numberPhone;
    private String nameCustomer;
    private Integer balance;
}
