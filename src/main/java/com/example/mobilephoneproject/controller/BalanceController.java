package com.example.mobilephoneproject.controller;

import com.example.mobilephoneproject.entity.BalancePhoneEntity;
import com.example.mobilephoneproject.service.BalancePhoneServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BalanceController {
    private final BalancePhoneServiceImpl balancePhoneService;

    @Autowired
    public BalanceController(BalancePhoneServiceImpl balancePhoneService) {
        this.balancePhoneService = balancePhoneService;
    }

@GetMapping(value = "/find-number-phoneById/{id}")
    public ResponseEntity<?> findPhoneNumberById(@PathVariable Integer id) {
    BalancePhoneEntity balancePhone = balancePhoneService.findById(id);

    if(balancePhone.getId() == null) {
        return ResponseEntity.ok().body(HttpStatus.NOT_FOUND);
    }
    return ResponseEntity.ok(balancePhone);
}

}
