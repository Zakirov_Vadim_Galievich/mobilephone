package com.example.mobilephoneproject.service;

import com.example.mobilephoneproject.dto.BalancePhoneDto;
import com.example.mobilephoneproject.entity.BalancePhoneEntity;
import com.example.mobilephoneproject.repository.BalansePhoneRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BalancePhoneServiceImpl implements BalanceService{
    private final BalansePhoneRepo balansePhoneRepo;

    @Override
    public BalancePhoneEntity findById(Integer id) {
        return (BalancePhoneEntity) balansePhoneRepo.findById(id).orElse(new BalancePhoneEntity());
    }
    @Override
    public List<BalancePhoneEntity> findAll() {
        List<BalancePhoneEntity> list = balansePhoneRepo.findAll();
        return list;
    }
    @Override
    public BalancePhoneEntity findByNumber(Integer number) {
        return (BalancePhoneEntity) balansePhoneRepo.findByNumber(number);
    }
    @Override
    public List<BalancePhoneEntity> findByName(String nameCustomer) {
        return balansePhoneRepo.findByName(nameCustomer);
    }
    @Override
    public void saveAll(List<BalancePhoneEntity> listBalance) {
        balansePhoneRepo.saveAll(listBalance);
    }
    @Override
    public void saveAlone(BalancePhoneEntity balancePhone) {
        balansePhoneRepo.save(balancePhone);
    }
    @Override
    public void deleteById(Integer id) {
        balansePhoneRepo.deleteById(id);
    }
    @Override
    public void addingMoneyToBalance(Integer phoneNumber, Integer sum) {
        BalancePhoneEntity byPhoneNummber = findByNumber(phoneNumber);
        byPhoneNummber.setBalance(byPhoneNummber.getBalance() + sum);
        saveAlone(byPhoneNummber);
    }
}
