package com.example.mobilephoneproject.service;

import com.example.mobilephoneproject.entity.BalancePhoneEntity;

import java.util.List;

public interface BalanceService {
    public BalancePhoneEntity findById(Integer id);
    public List<BalancePhoneEntity> findAll();
    public BalancePhoneEntity findByNumber(Integer number);
    public List<BalancePhoneEntity> findByName(String nameCustomer);
    public void saveAll(List<BalancePhoneEntity> listBalance);
    public void saveAlone(BalancePhoneEntity balancePhone);
    public void deleteById(Integer id);
    public void addingMoneyToBalance(Integer phoneNumber, Integer sum);
}
