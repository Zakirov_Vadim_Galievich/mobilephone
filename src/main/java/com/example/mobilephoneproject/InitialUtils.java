package com.example.mobilephoneproject;

import com.example.mobilephoneproject.entity.BalancePhoneEntity;
import com.example.mobilephoneproject.service.BalancePhoneServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Service
public class InitialUtils implements CommandLineRunner {
    private BalancePhoneServiceImpl balancePhoneServiceImpl;


    @Override
    public void run(String... args) throws Exception {
        List<BalancePhoneEntity> list = new ArrayList<>(
                Arrays.asList(
                        new BalancePhoneEntity().setId(1).setNumberPhone(555000).setBalance(100).setNameCustomer("Иван"),
                        new BalancePhoneEntity().setId(2).setNumberPhone(444000).setBalance(250).setNameCustomer("Мария"),
                        new BalancePhoneEntity().setId(3).setNumberPhone(111000).setBalance(60).setNameCustomer("Дмитрий")
                )
        );

        balancePhoneServiceImpl.saveAll(list);

        for(BalancePhoneEntity e : balancePhoneServiceImpl.findAll()){
            System.out.println(e);
        }
    }
}
